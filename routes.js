var request = require('request');

module.exports = function(app) {

	app.get('/api/q', function(req, res) {

		console.log("beginning request");

		request.post({url:'http://api.forismatic.com/api/1.0/', 
			form: {
				method: 'getQuote',
				format: 'json',
				lang: 'en'}}, 
			function(err, httpResponse, body)
			{
				if(err)
				{
					console.log(err);
					console.log("ERROR for getQuote");
					res.status(500).send({ error: "failed to send"});
				}
				else
				{
					console.log("sending response body");
					//res.status(200).send({body});
					res.send({body});
				}
			});
	});

	app.post('/api/w', function(req, res) {
		var lat = req.body.lat;
		var lon = req.body.lon;

		request('http://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + lon + '&appid=' + process.env.openweatherAppID, 
			function (error, response, body) {
			  	if (!error && response.statusCode == 200) 
			  	{
			    	 res.status(200).send({response});
			  	}
			  	else 
			  	{
			  		console.log(error + body);
					res.status(500).send({ error: "failed to send" });
				    return;
			 	}

		});

	});

	app.post('/api', function(req, res) {

		var emailBody = req.body.emailBody;
		var emailAddress = req.body.emailAddress;
		var emailHTML = req.body.emailHTML;

		var nodemailer = require('nodemailer');
		var sgTransport = require('nodemailer-sendgrid-transport');

		var options = {
		    auth: {
		        api_key: process.env.sendgridKey
		    }
		}

		var mailer = nodemailer.createTransport(sgTransport(options));

		var email = {
		    to: [emailAddress],
		    from: 'taskmailservice@gmail.com',
		    subject: 'TaskMail',
		    text: emailBody,
		    html: emailHTML,
		    timeout: 20000
		};

		mailer.sendMail(email, function(err, res) {
		    if (err) 
		    {       
		        console.log(err);
				res.status(500).send({ error: "failed to send" });
		        return;
		    }
		    else
		    {
		    	res.status(200).send({ success: "sent!" });
		    }
		});

		
		/*req.on('end', function() {
	    	console.log('success');
	    	res.send('success!!!');

	 	 });
		*/
	});

}