var express = require('express');
var morgan = require('morgan');
var methodOverride = require('method-override');
//var config = require('./config/config');

var app = express();
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST

/*app.use(express.static(__dirname + '/public', {redirect : false }));

var routes = require('./routes');

require('./config/express')(app, config);

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
    next();
    });

require('./routes')(app);

*/

app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());                                     // parse application/json
app.use(methodOverride());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
    next();
    });

require('./routes')(app);



app.get('/', function(req, res, next) {
    if(!req.secure) 
    {
        res.redirect(['https://', req.get('Host'), req.url].join(''));
    }

    else
    {
        res.render('./public/index.html', {
            title: 'TaskMail'
        });
    }
    next();

});


app.listen(process.env.PORT || 3000, function () {
  console.log('Express server listening on port ' + 3000);
});
