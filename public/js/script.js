var main = function()
{
    genQuote();

	getLocation();

	$("input#name").focus();  

	$('#taskForm').submit(function(event) 
	{
		var $input = $(event.target).find('input');
		var task = $input.val();
		if(task != "")
		{
			task += "      ";
			var html = $('<li>').text(task);
			//html.prependTo('#tasks');
			//deleteButton = $('<button />').addClass('deleteButton btn-danger').text('X');
			var $li = $('<li><span>' + task + '</span><button id="removeBtn" class="removeButton" type="button">-</button></li>');
			    /*$li.find('button').button({
			        icons: { primary: 'ui-icon-minusthick' },
			        text: true
			    });
			    */
			$li.prependTo('#tasks');
			//$("ul").append($li);
			$input.val("");
		}
		

		return false;
	});

	$('#tasks').on('click', '.removeButton', function() {
    	$(this).closest('li').remove();
	});

	$('#emailForm').submit(function(event)
	{
		var $input = $(event.target).find('input');
		var email = $input.val();
		
   		if (!isValidEmailAddress(email)) 
   		{
        	$("label#errorLabel").html('Invalid email');
       		$("input#email").focus();   
       		
      		return false;
 		}
		else
		{
			if ($('#tasks li').length != 0)
			{
				$("button#emailSubmitBtn").addClass('disabled');
	     		$("button#emailSubmitBtn").prop('disabled', true);
				processMessage(email);
				return false;
			}
			else 
			{
				$("label#errorLabel").html('You have not added any tasks');
       			$("input#name").focus();
       			return false;
			}
		}

	});




}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
};


function processMessage(emailAddress)
{
	var emailBody = "";
	var emailHTML = "";
	var listItems = $("#tasks li");
	listItems.each(function(index,li) {
    	
		emailBody += ((index+1) + ".  " + li.textContent.slice(0, -1) + "\n");
		emailHTML += '<b>' + ((index+1) + '.  </b>' + li.textContent.slice(0, -1) + '<br>');
	});

	 $.ajax({
             type: "POST",
             url: "api",
             data: JSON.stringify({emailAddress: emailAddress, emailBody: emailBody, emailHTML: emailHTML}),
             contentType: "application/json; charset=utf-8", 
             crossDomain: false,
             dataType: "text",
             success: function (data, status, jqXHR) {
             	var audio = new Audio('arrow-whoosh.ogg');
				audio.play();
				$("label#success").html('Email sent  <i class="fa fa-paper-plane" aria-hidden="true"></i>');
				$("button#emailSubmitBtn").prop('disabled', false);
				$("button#emailSubmitBtn").html('Submit');
				$("button#emailSubmitBtn").removeClass('disabled');
				$("label#errorLabel").html('');
             },

             error: function (jqXHR, status) {
                 // error handler
                 console.log(jqXHR);
                 $("label#success").html('');
                 $("label#errorLabel").html('Error sending!  Status code:  ' + status.code);
				 $("button#emailSubmitBtn").prop('disabled', false);
				 $("button#emailSubmitBtn").removeClass('disabled');
             }
    });


	return false;

};

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) { getWeather(position.coords.latitude, position.coords.longitude); });
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function getWeather(lat, lon) {

	$.ajax({
             type: "POST",
             url: "api/w",
             data: JSON.stringify({lat: lat, lon: lon}),
             contentType: "application/json; charset=utf-8", 
             crossDomain: false,
             dataType: "json",
             success: function (data, status, jqXHR) {
             	//get weather: main from this.  and current temp.  find how many dif types of weather. find pics.
             	var jsond = JSON.parse(data.response.body);
             	var iconURL = jsond.weather[0].icon;
             	var country = jsond.sys.country;
             	if(country === "US")
             	{
             		var fahrenheit = Math.round((jsond.main.temp * (9/5) - 459.67) * 100 / 100);
             		$("#weatherTemp").html(fahrenheit + '°  F');
             	}
             	else
             	{
             		var celsius = Math.round((jsond.main.temp - 273.15) * 100 / 100);
	             	$("#weatherTemp").html(celsius + '°  C');
             	}
             	$('#weatherCity').html(jsond.name + ',  ' + jsond.sys.country);
             	
             	$("#weatherImg").attr("src","http://openweathermap.org/img/w/" + iconURL  + ".png");
             },

             error: function (jqXHR, status) {
                 // error handler
                 console.log(jqXHR);
                 console.log(status);
             }
    });
}

function genQuote()
{
	$.ajax({
             type: "GET",
             url: "api/q",
             contentType: "application/json; charset=utf-8",
             crossDomain: false,
             dataType: "json",
             success: function (data, status, jqXHR) {
                console.log(data);
                console.log(data.body);
             	var jsond = JSON.parse(data.body);
             	$("#quote").html('<i>' + jsond.quoteText + '</i>');
             },

             error: function (jqXHR, status) {
                 // error handler
                 console.log(jqXHR);
                 console.log(status);
             }
    });
}

$(document).ready(main);